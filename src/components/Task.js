import React from 'react';
import { Form, Button } from 'react-bootstrap';

const Task = (props) => {
  const { id, taskName, isFinished, deleteTask, setStatus } = props;

  return (
    <>
      <Form.Check type="checkbox" id={id}>
        <Form.Check.Input
          type="checkbox"
          onChange={(e) => setStatus(id, e.target.checked)}
          checked={isFinished}
        />
        <Form.Check.Label>{taskName}</Form.Check.Label>
      </Form.Check>
      <Button variant="danger" onClick={() => deleteTask(id)}>
        Удалить
      </Button>
    </>
  );
};

export default Task;
