import React from 'react';
import {
  Button,
  InputGroup,
  Form,
  Container,
  Row,
  Col,
  Card,
} from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';
import List from './List';

class MyContainer extends React.Component {
  constructor() {
    super();

    this.taskLength = 7;
    this.state = {
      tasks: [
        {
          id: uuidv4(),
          name: 'Task1',
          isFinished: true,
        },
        {
          id: uuidv4(),
          name: 'Task2',
          isFinished: false,
        },
      ],
      validated: false,
      taskName: '',
    };

    this.deleteTask = this.deleteTask.bind(this);
    this.inputChange = this.inputChange.bind(this);
    this.createTask = this.createTask.bind(this);
    this.setStatus = this.setStatus.bind(this);
  }

  inputChange(e) {
    const { value } = e.target;
    this.setState({
      taskName: value,
      validated: value.length && value.length <= this.taskLength,
    });
  }

  createTask(e) {
    if (!this.state.validated) {
      return;
    }

    e.preventDefault();
    this.setState({
      tasks: [
        ...this.state.tasks,
        {
          id: uuidv4(),
          name: this.state.taskName,
          isFinished: false,
        },
      ],
      taskName: '',
      validated: false,
    });
  }

  deleteTask(id) {
    this.setState({
      tasks: [...this.state.tasks].filter((task) => task.id !== id),
    });
  }

  setStatus(id, status) {
    this.setState({
      tasks: [...this.state.tasks].map((task) => {
        if (task.id === id) {
          task.isFinished = status;
        }

        return task;
      }),
    });
  }

  render() {
    return (
      <Container className="mt-4">
        <Row className="justify-content-center">
          <Col xs={8}>
            <Card>
              <Card.Body>
                <Form onSubmit={this.createTask}>
                  <InputGroup>
                    <Form.Control
                      type="text"
                      placeholder="Введите название"
                      value={this.state.taskName}
                      isValid={
                        this.state.taskName.length &&
                        this.state.taskName.length <= this.state.taskLength
                      }
                      onChange={this.inputChange}
                    />
                    <InputGroup.Append>
                      <Button
                        variant="primary"
                        type="submit"
                        disabled={!this.state.validated}
                      >
                        Добавить запись
                      </Button>
                    </InputGroup.Append>
                  </InputGroup>
                </Form>
                {this.state.tasks.length !== 0 ? (
                  <List
                    tasks={this.state.tasks}
                    deleteTask={this.deleteTask}
                    setStatus={this.setStatus}
                  />
                ) : (
                  <p>Здесь ничего нет. Добавьте первую запись!</p>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default MyContainer;
