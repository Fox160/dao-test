import React from 'react';
import Task from './Task';
import { ListGroup } from 'react-bootstrap';

const List = (props) => {
  const { tasks, deleteTask, setStatus } = props;

  return (
    <>
      <ListGroup className="mt-3">
        {tasks.map((task) => (
          <ListGroup.Item
            key={task.id}
            className="d-flex justify-content-between align-items-center"
          >
            <Task
              id={task.id}
              taskName={task.name}
              isFinished={task.isFinished}
              deleteTask={deleteTask}
              setStatus={setStatus}
            />
          </ListGroup.Item>
        ))}
      </ListGroup>
    </>
  );
};

export default List;
