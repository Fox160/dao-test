import MyContainer from './components/Container';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return <MyContainer />;
}

export default App;
